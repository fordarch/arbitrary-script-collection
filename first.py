import psycopg2
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import datetime as dt
import time



SQL = """
select date_trunc('week', hi."date") "WO", sum(hours)
from  * FROM SCHEMA.TABLE
group by date_trunc('week', hi."date")
order by "WO" asc;
"""

'''
Return value is array of tuples
e.g.
[(datetime.datetime(2020, 6, 29, 0, 0, tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None)), Decimal('6'), Decimal('0'), Decimal('0')), ...]
'''

dates = []
input_data = []


# Connect to an existing database
with psycopg2.connect(dbname='DBNAME', user='USER', password='PASSWORD', host='1.1.1.1', port='5432') as conn:
    with conn.cursor() as cur:
        cur.execute(SQL)
        result = cur.fetchall()

        '''
        for res in result:
            for r in res:
                print(r)
        '''

        for row in result:
            #print(str(row[0]))
            #dates.append(str(row[0]))
            dates.append(row[0])

            #print(float(row[1]))
            input_data.append(float(row[1]))

        cur.close()

x = mdates.date2num(dates)
plt.style.use('ggplot')


y1 = np.array(input_data)
m1, b = np.polyfit(x, y1, 1)
p4 = np.poly1d(m1)
fig, cx = plt.subplots()


cx.bar(dates, y1, 5, color='g')
plt.plot(x, m1*x + b)
plt.title(f'TITLE')


plt.show()