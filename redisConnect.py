import redis


queried_value = None

try:
    # Generate the connection
    r = redis.Redis(host='192.168.1.117', port=6379)

    # Set and retrieve the same key
    r.set('test_key', 'This is a test value for showing redis connectivity')
    queried_value = r.get('test_key')
except Exception as e:
    print(f'Unable to connect or execute commands on Redis server: {e}')

# Print out queried value
print(queried_value.decode('utf-8'))