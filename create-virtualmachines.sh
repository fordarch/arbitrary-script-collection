#!/bin/bash 
set -e


# Assumes bootstrap.sh has been ran.

virt-install --name=linuxconfig-vm \
--vcpus=2 \
--memory=2048 \
--cdrom=/tmp/ubuntu-20.04.2-live-server-amd64.iso \
--disk size=20 \
--os-variant=ubuntu20.04