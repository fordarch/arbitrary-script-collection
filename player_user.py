#!/usr/bin/env python
import os, sys
import pika
import argparse
import queue


task_queue = queue.Queue()


class PlayerCharacter(object):
    hitPoints = 1000
    armor = 50




def Attack(attack, damage):
    print(f'under attack. attk: {attack} dmg: {damage}')
    if attack > PlayerCharacter.armor:            
        PlayerCharacter.hitPoints -= damage
        print(f'hit! new hp: {PlayerCharacter.hitPoints}')


def ProcessQueue(payload_key, payload_value):
    task_queue.put((payload_key, payload_value))


def Callback(ch, method, properties, body):
    rec = body.decode("utf-8").split(';')
    attack = int(rec[0])
    damage = int(rec[1])
    Attack(attack, damage)


def Listener(targethost, messagequeue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=targethost))
    channel = connection.channel()
    channel.queue_declare(queue=messagequeue)
    channel.basic_consume(queue=messagequeue, on_message_callback=Callback, auto_ack=True)
    channel.start_consuming()


def ParseArguments():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-m', '--mqip', type=str, required=True, help='')
    parser.add_argument('-q', '--mqname', type=str, required=True, help='')
    return parser.parse_args()


def Main():
    args = ParseArguments()
    Listener(args.mqip, args.mqname)



if __name__ == '__main__':
    try:
        Main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)