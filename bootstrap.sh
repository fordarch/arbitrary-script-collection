#!/bin/bash
set -e


########################################
# Description
########################################
# Will bootstrap the installation of what will become a 3-node K8 installation.
# Expected to be ran immediately after the fresh install of a new operating system.
# OS tested
#   Ubuntu 20.04 LTS x64
# Expected to be ran as a SUDOER
# Script pulls source material from internet (apt packages, git repositories, etc)
# Executing user with dominion over virtual machine management.


########################################
# Init
########################################
# --------------------------------------
# Style
# --------------------------------------
RED='\033[0;31m'
GREEN='\033[0;32m'
LTCYAN='\033[1;36m'
NC='\033[0m' # No Color

# --------------------------------------
# All the directories
# --------------------------------------
BOOTSTRAP_WORKSPACE_DIR='/opt/bootstrapper'
ANSIBLE_PLAYBOOK_DIR='/opt/pb'


########################################
# Environment
########################################
# --------------------------------------
# Initial checks to see if execution of this script will work.
# --------------------------------------
user_id=`id -u`

# must run as sudo
if [ $user_id -ne 0 ]; then
    echo "Must run as sudo"
    exit 1
fi

# --------------------------------------
# Get the underlying operating system and file structure up-to-date
# --------------------------------------
sudo apt update

sudo apt upgrade -y

# Tool to check system readyness for virtualization.
sudo apt install cpu-checker


# --------------------------------------
# The system should have either an Intel processor with the VT-x (vmx), 
# or an AMD processor with the AMD-V (svm) technology support.
# --------------------------------------
if [[ ! `grep -Eoc '(vmx|svm)' /proc/cpuinfo` > 0 ]]
then 
    echo -e "${RED}ERROR${NC}: Failed check. Virtualization enabled flag set on CPU. The system should have either an Intel processor with the VT-x (vmx), or an AMD processor with the AMD-V (svm) technology support."
    exit 1
fi

# --------------------------------------
# Check virtualization readyness of system BIOS
# --------------------------------------
if [[ ! kvm-ok -eq 0 ]] 
then 
    echo "{RED}ERROR${NC}: Failed check. Virtualization enabled in system BIOS."
    exit 1
fi


########################################
# System Prep
########################################
# Carve out a nice isolated directory to work out of
[[ ! -d $ANSIBLE_PLAYBOOK_DIR ]] && mkdir -p -- $BOOTSTRAP_WORKSPACE_DIR

# /opt/pb will be the place to store ansible playbooks
#[[ ! -d $ANSIBLE_PLAYBOOK_DIR ]] && mkdir -p -- $ANSIBLE_PLAYBOOK_DIR

#cd /opt/pb/


########################################
# Installations
########################################
# --------------------------------------
# KVM
# --------------------------------------
# Install all applications and dependancies
apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virtinst virt-manager

# Wait for libvirtd to start
if [[ ! $(systemctl is-active libvirtd) == 'active' ]] 
then 
    echo "Waiting for libvirtd to start. Allow short wait and give it one more try."
    sleep 2
    if [[ ! $(systemctl is-active libvirtd) == 'active' ]] 
    then 
        echo "{RED}ERROR${NC}: Failed check. Daemon libvirtd is not in active state. See command 'systemctl is-active libvirtd'."
        exit 1
    fi
fi

# Equip executing user with dominion over virtual machine management.
usermod -aG libvirt $USER
usermod -aG kvm $USER

# --------------------------------------
# Ansible
# --------------------------------------
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt-get install ansible -y


#do more thing with ansible


