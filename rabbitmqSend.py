#!/usr/bin/env python
import pika


connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='testqueue')
channel.basic_publish(exchange='', routing_key='testqueue', body='1000;1000')

connection.close()