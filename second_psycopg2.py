#!/usr/bin/python3

import sys
import argparse
import psycopg2 
import json


def GetConfigData(inputFile, passwordfile):
    """ Parse input file
        Use result from input file to execute queries
        Write results to output file destination
    """
    passworddict = []
    jsondata = []

    # Get our dict of password for later lookup
    #  inputFile should never contain plain-text passwords, just the keymap val
    with open(passwordfile) as f:
        passworddict = json.load(f)

    # Open input file. Can contain 1+ elements in a list.
    # Each element contains a list of related queries and the info to get the data.
    with open(inputFile) as f:
        jsondata = json.load(f)

    return jsondata, passworddict


def ProcessQueryRequests(jsondata, allCredentialSets):
    retval = {}

    for elem in jsondata:
        uuid = elem.get('uuid')
        query_list = elem.get('query_list')
        current_tag = elem.get('tag')
        schema = elem.get('schema')
        table = elem.get('table')

        retval[current_tag] = []

        # Missing basic fields in the config file? Then quit out.
        if None in (uuid, query_list):
            print('UUID or query_list was empty and we cannot proceed.')
            exit(1)

        # allCredentialSets structure (list of dict)
        # [{'a': 1, 'b': 10}, {'a': 2, 'b': 20}]
        matchingCredSets = [recordset for recordset in allCredentialSets if recordset['uuid'] == uuid]

        if len(matchingCredSets) != 1:
            if(matchingCredSets < 1):
                print('Query failed to pair with any credentials.')
                print(f'json data: {jsondata}')
                print(f'allcredset: {allCredentialSets}')
            else:
                print('Query matched multiple credentials. Likely issue with credential config.')
                print(f'Matches: {matchingCredSets}')
            exit(1)

        creds = matchingCredSets[0]

        db_user = creds.get('name')
        db_pass = creds.get('pass')
        db_host = creds.get('dbhost')
        db_port = 5432 # pgsql standard port

        # Check if user specified non-standard port
        if ':' in db_host:
            db_port = db_host.split(':')[1]
            db_host = db_host.split(':')[0]

            try:
                db_port = int(db_port)
            except Exception as e:
                retval = [-1]
                retval.append(f'Bad port value specified from provided host value: {db_host}. Exception: {e}')

            if db_port < 0 or db_port > 65535:
                retval.append(f'Database port value is out of range (0-65535): {db_port}')
                return retval

        db_instance_name = creds.get('instance_name')

        # Detects if any key/field was not provided, not keys with empty values
        if None in (db_user, db_host, db_pass, db_instance_name):
            exit(1)

        print()
#       TODO: ADD JSON SYNTAX CHECK. QUERY ENDING WITH ';' WILL YEILD CONFUSING ERROR:
#           "TypeError: 'int' object is not iterable"
        for q in query_list:
            # Format query string with the expected begin and end timestamps.
            fQuery = q.format(schema, table)

            print("++++++++++ PROCESS QUERY ++++++++++")
            print(f"SQL>{fQuery}")
            print()

            qresult = ExecuteQuery(fQuery, db_host, db_port, db_user, db_pass, db_instance_name)

            # Bad query check. 
            if qresult[0] == -1:
                print(f'Bad query return. {qresult}')
                continue

            print("++++++++++ RESULT ++++++++++")

            for elem in qresult:
                print(f"{elem} ")

            for elem in qresult:
                if len(elem) > 0:
                    retval[current_tag].append(elem)

    print("++++++++++ RETURN VALUE ++++++++++")
    print(len(retval))
    print(retval)

    return retval



def ExecuteQuery(query, db_host, db_port, db_user, db_pass, db_instance_name):
    """ This guy executes our query and returns the result.
        Return format is list of tuples
        e.g. [(a,1),(b,2)]
        e.g. [(datetime.datetime(2020, 10, 23, 20, 8, 28, 692000), 'F016;;8;LREP;1;6303633;;F16POSTPACK;;[]'), ...]
    """
    qresult = []

    conn_string = f"host='{db_host}' dbname='{db_instance_name}' user='{db_user}' password='{db_pass}'"

    try:
        with psycopg2.connect(conn_string, port=db_port) as connection:
            cursor = connection.cursor()
            cursor.execute(query)
            for r in cursor.fetchall():
                qresult.append(r)
    except Exception as e:
        qresult = [-1]
        qresult.append(f'An unexpected exception occurred while connecting to the target database: {e}')

    return qresult


def Main(inputFile, destination, passwordfile):
    jsondata, passworddict = GetConfigData(inputFile, passwordfile)
    return ProcessQueryRequests(jsondata, passworddict)


def GetArgs():
    parser = argparse.ArgumentParser(description='Process args for online backup log duration.')
    parser.add_argument('-f', '--inputFile', required=True, action='store', help='Filepath for json input file.')
    parser.add_argument('-d', '--destination', required=True, action='store', help='Filepath for execution output.')
    parser.add_argument('-p', '--passwordfile', required=True, action='store', help='Filepath for password file.')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    """ Sample call
        $python3 pgget.py --inputFile "/path/to/input.json" --destination "/path/to/output/" --passwordfile "/path/to/password.txt"
    """
    args = GetArgs()
    Main(args.inputFile, args.destination, args.passwordfile)